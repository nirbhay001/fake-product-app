import './App.css';
import React, {Component} from 'react';
import Header from './Components/Header/Header';
import Products from './Components/Products/Products';

class App extends Component {
  render(){
    return (
      <div>
        <Header/>
        <Products/>
      </div>
    );
  }

}

export default App;
