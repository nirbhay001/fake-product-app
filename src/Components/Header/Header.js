import React, { Component } from "react";
import "./Header.css";

class Header extends Component {
  render() {
    return (
      <>
        <header className="header">
          <span className="home-button">
            <i className="fa-solid fa-house cart-icon"></i>
          </span>
          <div className="product-type">
            <span className="product-link">Home</span>
            <span className="product-link">Electronics</span>
            <span className="product-link">Grocerry</span>
          </div>
          <div className="cart-icon">
            <i className="fa-solid fa-cart-shopping"></i>
          </div>
        </header>
      </>
    );
  }
}

export default Header;
