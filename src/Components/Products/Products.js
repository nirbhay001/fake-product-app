import React, { Component } from "react";
import Card from "../Card/Card";
import "./Products.css";
import { InfinitySpin } from "react-loader-spinner";

class Products extends Component {
  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
      errorMessage: "",
    };
  }

  componentDidMount() {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((productData) =>
        this.setState({
          ...this.state,
          products: productData,
          status: this.API_STATES.LOADED,
        })
      )
      .catch((error) => {
        console.error(error.message);
        this.setState({ ...this.state, status: this.API_STATES.ERROR });
      });
  }
  render() {
    const { products, status } = this.state;
    return (
      <>
        {status === this.API_STATES.LOADING ? (
          <div className="spinner">
            <InfinitySpin width="200" color="#4fa94d" />
          </div>
        ) : status === this.API_STATES.ERROR ? (
          <h1 className="data-flag">Error occured in fetching the data </h1>
        ) : products.length === 0 ? (
          <h1 className="data-flag">Data not Found</h1>
        ) : (
          <div className="products">
            {this.state.products.map((product) => {
              return <Card product={product} key={product.id} />;
            })}
          </div>
        )}
      </>
    );
  }
}

export default Products;

